
const FIRST_NAME = "Alexandru";
const LAST_NAME = "Tuliga";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
   
    let db = new Object();
    
    return {
        pageAccessCounter: function( pageName ){
            
             if( pageName === undefined )
                pageName = 'home';
 
             pageName = pageName.toLowerCase();
            
             if( pageName in db ){
                 db[pageName]++;
             } else {
                 db[pageName] = 1;
             }
            
        },
        getCache: function(){
            return db;
        }
    };
    
 }
 

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

